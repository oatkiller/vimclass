VIM has registers, think of these like your nubby clipboard. 

The y command is for "yanking" text. It's like copying. It will save the selected text into the default register.

The p command is for pasting. You can use it to paste text from the default register. It even works with the visual block mode.

Try copying this line and pasting it below. The command is Vyp. There is a shortcut for this, yyp

The default register gets the value of the last deleted text as well. Press x to delete a character and p to paste in. You can use this to switch two characters. Try making xp say px.

Try selecting several lines and deleting them, moving your cursor, and pasting with p. The command should be something like Vhhdkkkp
